import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import static java.awt.Color.*;

public class Main {
    public final static int firstX = 146, firstY = 589;
    public final static int step = 72,
            minNumberWidth = 30,  minNumberHeight = 27,
            signShiftX = 0, signShiftY = minNumberHeight,
            minSignHeight = 16, minSignWidth = 18, NOT_FOUND = -1;
    public static final double MIN_WELL_MATCH = 0.96;
    static HashMap<String, HashMap<String, BufferedImage>> names = new HashMap<>();
    static HashMap<String, HashMap<String, BufferedImage>> signs = new HashMap<>();
    private static String root, resultsDir;

    private static void loadExamples(String dir, HashMap<String, HashMap<String, BufferedImage>> map) throws IOException {
        File p = new File(root + dir);
        Files.createDirectories(p.toPath());
        for (File f : p.listFiles()) {
            HashMap<String, BufferedImage> pictures = new HashMap<>();
            map.put(f.getName(), pictures);
            for (File f2 : f.listFiles()) {
                try {
                    pictures.put(f2.getName(), ImageIO.read(f2));
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            long startTime = System.currentTimeMillis();
            root = System.getProperty("user.dir") + System.getProperty("file.separator");
            loadExamples("names", names);
            loadExamples("signs", signs);
            if (args.length > 1) {
                resultsDir = args[1];
            }
            File[] files = new File(args[0].replace("\"", "")).listFiles();
            for (File f : files) {
                try {
                    int x = firstX, y = firstY;
                    BufferedImage img = ImageIO.read(f);
                    System.out.print(f.getName() + " - ");
                    while (x < img.getWidth() - step) {
                        Point point = findCard(img, x, y);
                        if (point == null) {
                            break;
                        } else {
                            String fName = x + "_" + f.getName();
                            String name = findTheSame(fName,
                                    findLeftTop(img, point.x, point.y, minNumberWidth, minNumberHeight),
                                    names, "names", false);
                            String sign = findTheSame(fName,
                                    findLeftTop(img, point.x + signShiftX, point.y + signShiftY, minSignWidth, minSignHeight),
                                    signs, "signs", true);
                            System.out.print(name + sign);
                            if (resultsDir != null) {
                                save(img.getSubimage(point.x, point.y, 60, 90), resultsDir + name + "/" + sign, fName);
                            }
                            x += step;
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                System.out.println();
            }
            long time = (System.currentTimeMillis() - startTime);
            System.out.println(files.length + " files were processed by " + time / files.length + " ms for each file.");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private static String findTheSame(String fName, BufferedImage img, HashMap<String, HashMap<String, BufferedImage>> examples, String dir, boolean checkColor) throws IOException {
        Set<Map.Entry<String, HashMap<String, BufferedImage>>> set = examples.entrySet();
        double best = 0;
        Map.Entry<String, HashMap<String, BufferedImage>> ret = null;
        for (Map.Entry<String, HashMap<String, BufferedImage>> entry : set) {
            if (entry.getValue().keySet().contains(fName)) {
                return entry.getKey();
            }
            for (BufferedImage img1 : entry.getValue().values()) {
                double result = compare(img, img1, checkColor);
                if (result > best) {
                    ret = entry;
                    if (result > MIN_WELL_MATCH) {
                        return entry.getKey();
                    }
                    best = result;
                }
            }
        }
        if (ret == null) {
            save(img, "unknown_" + dir, fName);
            return "*** Please recognize unknown_" + dir + "/" + fName + " ***";
        } else {
            ret.getValue().put(fName, img);
            save(img, dir + "/" + ret.getKey(), fName);
            return ret.getKey();
        }
    }

    private static void save(BufferedImage img, String dir, String fileName) throws IOException {
        File p = new File(root + dir);
        Files.createDirectories(p.toPath());
        ImageIO.write(img, "png", new File(p.getAbsolutePath() + "/" + fileName));
    }

    public static Point findCard(BufferedImage img, int x0, int y0) {
        int xWhiteLine = NOT_FOUND;
        for (int x = x0; x < img.getWidth(); x++) {
            int rgb = img.getRGB(x, y0);
            if (checkColor(rgb) == WHITE) {
                if (xWhiteLine == NOT_FOUND) {
                    xWhiteLine = x;
                }
            } else {
                if (xWhiteLine != NOT_FOUND) {
                    int whiteLineWidth = x - xWhiteLine;
                    if ((whiteLineWidth > minNumberWidth) && (whiteLineWidth < step)) {
                        return new Point(xWhiteLine, y0);
                    }
                    xWhiteLine = NOT_FOUND;
                }
            }
        }
        return null;
    }

    protected static Color checkColor(int rgb) {
        int r = (rgb >> 16) & 0x000000FF;
        int g = (rgb >> 8) & 0x000000FF;
        int b = (rgb) & 0x000000FF;
        int avg = (r + g + b) / 3;
        if (avg > 100) {
            if (r > avg + 30) {
                return Color.RED;
            } else {
                return WHITE;
            }
        } else {
            if (r > avg + 15) {
                return Color.RED;
            } else {
                return Color.BLACK;
            }
        }
    }

    public static BufferedImage findLeftTop(BufferedImage img, int x0, int y0, int minWidth, int minHeight) {
        int left = Integer.MAX_VALUE, top = Integer.MAX_VALUE;
        for (int x = x0; x < x0 + minWidth; x++) {
            for (int y = y0; y < y0 + minHeight; y++) {
                Color color = checkColor(img.getRGB(x, y));
                if (color != WHITE) {
                    if (left > x) {
                        left = x;
                    }
                    if (top > y) {
                        top = y;
                    }
                }
            }
        }
        return img.getSubimage(left, top, minWidth, minHeight);
    }

    public static double compare(BufferedImage img, BufferedImage img1, boolean checkColor) {
        int equals = 0;
        int count = 0;
        for (int y = 0; y < img.getHeight(); y++) {
            for (int x = 0; x < img.getWidth(); x++) {
                Color c0 = checkColor(img.getRGB(x, y));
                if (c0 != WHITE) {
                    Color c1 = checkColor(img1.getRGB(x, y));
                    if (c1 != WHITE) {
                        if (checkColor) {
                            if (c1 != c0) {
                                return 0.0;
                            }
                        }
                        equals++;
                    } else {
                        equals--;
                    }
                    count++;
                } else if (checkColor(img1.getRGB(x, y)) != WHITE) {
                    equals--;
                }
            }
        }
        return (double) equals / count;
    }
}
